#3. Fes un programa que imprimeixi el teu horari de classe.

print("MI HORARIO DE CLASE:",end="\n"*2) #Para hacer una separación de líneas vacías introduces \n"*2 (el 2 es de ejemplo).

print ("horas","LUNES" ,"MARTES","MIÉRCOLES", "JUEVES" ,"VIERNES", sep="\t"*3)
print("8:00  - 09:00","nada","M8","M14"," M8","sin clase",sep="\t"*3,end="\n")
print("09:00 - 10:00","nada","M8","M14","M8","sin clase",sep="\t"*3,end="\n")
print("10:00 - 11:00","nada","vacío","vacío"," M8","EIE",sep="\t"*3,end="\n")
print("11:30 - 12:30","PATIO","PATIO","PATIO","PATIO","PATIO",sep="\t"*3,end="\n")
print("12:30 - 13:30 ","nada","REDES","REDES","TUTORÍA","REDES",sep="\t"*3 ,end="\n")
print("13:30 - 14:30 ","nada","REDES","REDES","EIE","REDES",sep="\t"*3 ,end="\n")
