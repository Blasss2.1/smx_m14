#9. Fes un programa que calculi l’àrea d’un cercle. 
# L’usuari ha d’introduir el ràdio. La fórmula és PI multiplicat per ràdio al quadrat(3.14 · r^2 ).

radio = float(input("Introduce el rádio del círculo: "))
Area = 3.1416 *  pow(radio,2)
print("El area es {:.2f}".format(Area))
print("El area es", round(Area,2))
