#print la palabra reservada para imprimir.


from os import sep


print("hola mundo")
print("hola, mundo")
print("hola", "mundo")  #la función print acepta varios parámetros.

print("hola" "mundo")#Varios parámetros sin separadores.


print("¡Hola, Mundo!")
print("Gala") #se produce un error de sintaxis de escritura.

print ("Gala") #te salta el error de falta de un paréntesis.En python2 funciona en python3.10 no.

print('Gala') #no te salta ningún error es una cadena

#separadores 
print("gala","blas",sep="") #el separador es el espacio pero aquí no es

print("gala","blas",sep="_",end="\t") #el separador es un espacio pero aquí no es ninguno.
#\n salto de linea
#\t tabulador
print("gala","blas",sep="_",end="@") 
print("mi edad es 20")

print("Fundamentos" , "Programación",sep="***" ,end="***")
print("en" , "Python",sep="...",end="")
