#pregunta 2 quin es el resultat de la següent comparació?

print (2 == 2 ) #Dos enteros mismo número iguales

print (2 == 2. ) #true

print ( 1 == 2) #Dos enteros diferentes números  #false

var = 0
print (var !=0) #false
var = 1
print (var !=1) #false

ovejasnegras = 10
ovejasblancas = 5 
print (ovejasnegras > ovejasblancas) #true

ovejasnegras = 10
ovejasblancas = 10
print (ovejasnegras > ovejasblancas) #false

#ejemplo de if

edad = int(input("Cuantos años tienes?"))
if edad < 18:
    print("eres menor de edad.")
if edad >= 18:
    print("eres mayor de edad.")
print("Adios!")

#ejemplo de if y else

edad = int(input("Cuantos años tienes?"))
if edad < 18:
    print("eres menor de edad.")
else:
    print("eres mayor de edad")
print("Adiós!")


#sentencias condiciones anidadas

print("Piensa un numeri entre 1 y 4:")
print("Contesta S (si) o N (no) a las siguientes preguntas:")

primera = input("el numero es mas grande que 2?")
if primera == "S":
    segunda = input("el numero es mas grande que 3?")
    if segunda == "S":
        print("el numero pensado es 4.")
    else:
        print("el numero pensado es 3.")
else:
    segunda = input("el numero es mas grande que 1?")
    if segunda == "S":
        print("el numero pensado es 2.")
    else:
        print("el numero pensado es 1.")
    print("Adiós!")