#opción 2
#cambiar la condición a major que para que deje de hacer un bucle.
contador = 5
while contador > 0:
    print("Dentro del ciclo", contador)
    contador += 1
print("Fuera del ciclo", contador)


#opción 1 ejecuta 5 veces

contador = 5
while contador != 0:
    print("Dentro del ciclo", contador)
    contador -= 1
print("Fuera del ciclo", contador)


#opción 3
contador = 5
while contador >= 0:
    print("Dentro del ciclo", contador)
    contador -= 1
print("Fuera del ciclo", contador)
